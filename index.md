---
title: Home
banner_image: "/uploads/2018/05/17/NJ_Vanleeuwen_09.jpg"
layout: landing-page
heading: DesignTen
partners:
- "/uploads/2017/11/13/stem.png"
- "/uploads/2017/11/13/UPenn_logo.png"
- "/uploads/2017/11/13/nysed.png"
services:
- description: Performing collaborative research and providing services to support
    the Health Sector.
  heading: Health
  icon: "/uploads/2017/11/13/health.png"
- description: Performing collaborative research and providing services to support
    the biotechnology sector.
  heading: BioTech
  icon: "/uploads/2017/11/13/biotech.png"
sub_heading: 10 Ringgit 1 Design
textline: 'DesignTen, world leading design firm '
hero_button:
  text: Learn more
  href: "/about"
show_news: true
menu:
  navigation:
    identifier: _index
    url: "/"
    weight: 1
---
